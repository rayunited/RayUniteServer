import com.google.gson.Gson
import java.net.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentMap
import kotlin.concurrent.thread
import kotlin.random.Random

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
fun main(args: Array<String>) {
    if (!args.contains("-rayunite-server-port")) {
        println("-rayunite-server-port command line argument not found")
        return
    } else {
        val cmdIndex = args.indexOf("-rayunite-server-port")
        val serverPort = args[cmdIndex + 1].toInt()

        val server = ServerSocket(serverPort)
        val datagramSocket = DatagramSocket(serverPort)

        println("Server is running on port ${server.localPort}")

        val gson = Gson()

        while (true) {
            val client = server.accept()
            println("Client connected: ${client.inetAddress.hostAddress}")

            val id = Random.nextInt(0, 1000)
    
            sendQueues[id] = ConcurrentLinkedQueue<ByteArray>()
            udpSendQueues[id] = ConcurrentLinkedQueue<ByteArray>()

            sendQueues[id]?.add(
                byteArrayOf(0, TCPPlayerIdentifierFromServer.TYPE_CODE.toByte())
                    .plus(
                gson.toJson(TCPPlayerIdentifierFromServer(identifier = id)) .toByteArray()))

            sendingDatagramSockets[id] = DatagramSocket()
            sendingDatagramSockets[id]?.connect(InetAddress.getLoopbackAddress(), client.port)

            // Run client in it's own thread.
            thread { ClientHandler(client, datagramSocket, id).run() }
            thread { ClientHandler(client, datagramSocket, id).runSending() }

            thread { ClientHandler(client, datagramSocket, id).runUDPSending() }
            thread { ClientHandler(client, datagramSocket, id).runUDP() }
        }
    }
}

class TCPPlayerIdentifierFromServer(val identifier: Int) {
    companion object {
        val TYPE_CODE = 3
    }
}

val sendQueues: ConcurrentMap<Int, ConcurrentLinkedQueue<ByteArray>> = ConcurrentHashMap()
val udpSendQueues: ConcurrentMap<Int, ConcurrentLinkedQueue<ByteArray>> = ConcurrentHashMap()
val sendingDatagramSockets: ConcurrentMap<Int, DatagramSocket> = ConcurrentHashMap()

class ClientHandler(client: Socket, datagramSocket: DatagramSocket, id: Int) {
    private val client: Socket = client
    private val datagramSocket: DatagramSocket = datagramSocket
    private val id: Int = id
    private var running: Boolean = false

    fun run() {
        running = true

        val buff = ByteArray(1024)
        while(client.inputStream.read(buff, 0, buff.size) > -1) {
            sendQueues.forEach {
                it.value.add(buff.copyOf())
            }
            //Thread.sleep(10)
        }
    }

    fun runSending() {
        running = true

        while (running) {
            val messageToSend = sendQueues[id]?.poll()
            if (messageToSend != null) {
                client.outputStream.write(messageToSend)
            }
            //Thread.sleep(10)
        }
    }

    fun runUDP() {
        running = true
        val buff = ByteArray(1024)
        val datagramPacket = DatagramPacket(buff, buff.size)
        //datagramSocket.connect(client.remoteSocketAddress)
        while(running) {
            //println("Receiving UDP from ${client.remoteSocketAddress}")
            datagramSocket.receive(datagramPacket)
            //println("Received UDP!")
            udpSendQueues.forEach {
                it.value.add(datagramPacket.data.copyOf())
            }
           // Thread.sleep(10)
        }
    }

    fun runUDPSending() {
        running = true
        while (running) {
            val messageToSend = udpSendQueues[id]?.poll()
            if (messageToSend != null) {
                sendingDatagramSockets[id]?.send(DatagramPacket(messageToSend, messageToSend.size))
                //println("Sent UDP to ${sendingDatagramSockets[id]?.remoteSocketAddress}!")
            }
            //Thread.sleep(10)
        }
    }
}